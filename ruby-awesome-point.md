The awesome ruby multiple string:

``` ruby
  app_file "config/routes.rb", <<-RUBY
      Rails.application.routes.draw do
          get 'foo', to: 'foo#bar'
          get 'custom', to: 'foo#custom'
          get 'mapping', to: 'foo#mapping'
          direct(:custom) { "http://www.microsoft.com" }
          direct(class: "User") { "/profile" }
     end
RUBY
```
Syntax:
<<NAME
  string in here
  ....
  ...
NAME
